/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package blorbReader;

import gameMemory.CMemHandler;
import gameMemory.MemorySegment;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Random;

class CMemHandlerTest {
	static MemorySegment gameData;

	@BeforeAll
	static void beforeAll() {
		UlxReader game;
		try {
			FileChannel fileChannel = FileChannel.open(
					Path.of(CMemHandlerTest.class.getResource("/Unit Tests/glulxercise.ulx").toURI()));
			game = new UlxReader(MemorySegment.fromBuffer(fileChannel.map(FileChannel.MapMode.READ_ONLY, 0,
					fileChannel.size())));
		} catch (IOException | URISyntaxException e) {
			Assertions.fail("Failed to open test game.");
			return; //Unreachable
		}
		gameData = game.getGameData();
	}

	@Test
	void duplicate() {
		MemorySegment currentGame = MemorySegment.fromSize(gameData.length());
		currentGame.put(gameData.rewind());
		MemorySegment save = CMemHandler.compressSave(gameData, currentGame);
		for (int i = 0; i < ((save.length() >> 8) << 8); i += 2) {
			Assertions.assertEquals(0, save.getByte(i), "" + i);
			Assertions.assertEquals(-1, save.getByte(i + 1), "" + i);
		}
		Assertions.assertEquals(0, save.getByte(save.length() - 2), "" + (save.length() - 2));
		Assertions.assertEquals(((byte) (gameData.length() % 255)), save.getByte(save.length() - 1), "" + (save.length() - 1));

		MemorySegment dest = MemorySegment.fromSize(gameData.length());
		CMemHandler.decompressSave(gameData, save, dest);
		for (int i = 0; i < gameData.length(); i++) {
			Assertions.assertEquals(gameData.getByte(i), dest.getByte(i), "" + i);
		}
	}

	@Test
	void real() {
		MemorySegment test;
		try {
			test = MemorySegment.fromBuffer(ByteBuffer.wrap(Files.readAllBytes(
					Path.of(CMemHandlerTest.class.getResource("/Unit Tests/umem.jluxs").toURI()))));
		} catch (IOException | URISyntaxException e) {
			Assertions.fail(e);
			return;
		}
		compareCompressedToTestSegment(test);
	}

	@Test
	void random() {
		MemorySegment test = MemorySegment.fromSize(gameData.length());
		Random r = new Random();
		test.put(r.ints().limit(gameData.length() / 4).toArray());

		compareCompressedToTestSegment(test);
	}

	private void compareCompressedToTestSegment(MemorySegment test) {
		MemorySegment save = CMemHandler.compressSave(gameData, test);
		MemorySegment dest = MemorySegment.fromSize(Math.max(test.length(), gameData.length()));
		CMemHandler.decompressSave(gameData, save, dest);
		for (int i = 0; i < test.length(); i++) {
			Assertions.assertEquals(test.getByte(i), dest.getByte(i), "" + i);
		}
	}
}