module BlorbReader {
	requires static org.jetbrains.annotations;
	requires static com.github.spotbugs.annotations;
	requires java.desktop;
	requires javafx.graphics;
	requires GameMemory;
	exports blorbReader;
}