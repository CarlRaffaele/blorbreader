/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package blorbReader;

import gameMemory.MemorySegment;
import javafx.scene.image.Image;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Unmodifiable;

import javax.sound.sampled.AudioInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

import static blorbReader.BlorbChunkType.*;

/**
 * A class to read a .blorb file.
 */
@SuppressWarnings("unused")
public class BlorbReader {
	@NotNull @Unmodifiable private final Map<BlorbChunkType, Map<Integer, Chunk>> fileIndex;
	@NotNull @Unmodifiable private final List<BlorbChunkType> optionalFieldsUsed;

	/**
	 * Create the file from the given file index.
	 *
	 * @param fileIndex The file index for the files in this blorb.
	 */
	private BlorbReader(@NotNull Map<BlorbChunkType, Map<Integer, Chunk>> fileIndex) {
		this.fileIndex = Collections.unmodifiableMap(fileIndex);
		this.optionalFieldsUsed = fileIndex.keySet()
										   .stream()
										   .filter(BlorbReader::isOptionalType)
										   .collect(Collectors.toUnmodifiableList());
	}

	@NotNull
	public static BlorbReader fromFile(@NotNull Path path) {
		ByteBuffer fileData;
		try (FileInputStream is = new FileInputStream(path.toFile())) {
			fileData = ByteBuffer.allocateDirect((int) is.getChannel().size());
			is.getChannel().read(fileData);
		} catch (IOException e) {
			throw new IllegalArgumentException("Could not open file to create blorb", e);
		}
		fileData.rewind();

		return fromBytes(fileData);
	}

	/**
	 * Create a {@link BlorbReader} from the given file. It assumes the {@link ByteBuffer}'s position is at the start
	 * of the blorb.
	 *
	 * @param fileData The file to parse.
	 * @return A BlorbReader object with all of the file information parsed out.
	 */
	@NotNull
	@Contract("_ -> new")
	public static BlorbReader fromBytes(@NotNull ByteBuffer fileData) {
		if (fileData.getInt() != FORM.getByteRepresentation() ||
				fileData.getInt() != fileData.capacity() - 8 ||
				fileData.getInt() != IFRS.getByteRepresentation()) {
			throw new IllegalArgumentException("Blorb doesn't start with form or has the wrong length");
		}

		Map<BlorbChunkType, Map<Integer, Integer>> fi = parseFileIndex(fileData);
		return new BlorbReader(collectChunkFromIndex(fileData, fi));
	}

	@NotNull
	private static Map<BlorbChunkType, Map<Integer, Chunk>> collectChunkFromIndex(@NotNull ByteBuffer data,
																				  @NotNull Map<BlorbChunkType, Map<Integer, Integer>> fileIndex) {
		Map<BlorbChunkType, Map<Integer, Chunk>> ret = new EnumMap<>(BlorbChunkType.class);
		fileIndex.forEach((key, value) -> {
			ret.put(key, new HashMap<>());
			value.forEach((key2, value2) -> ret.get(key).put(key2,
					new MemorySegmentChunk(MemorySegment.fromBuffer(data.position(value2).slice()))));
		});
		return ret;
	}

	/**
	 * Parse the file index. Assumes the ByteBuffer is already in the correct location. The hashmap it returns is in the
	 * format: map<"index area",map<itemID, memoryLocation>>
	 */
	@NotNull
	private static Map<BlorbChunkType, Map<Integer, Integer>> parseFileIndex(@NotNull ByteBuffer data) {
		if (data.getInt() != RESOURCE_INDEX.getByteRepresentation()) {
			throw new IllegalArgumentException("Buffer was not located at the start of the RIdx chunk");
		}

		EnumMap<BlorbChunkType, Map<Integer, Integer>> ret = new EnumMap<>(BlorbChunkType.class);
		//Define the mandatory sections
		ret.put(PICTURE, new HashMap<>());
		ret.put(SOUND, new HashMap<>());
		ret.put(DATA, new HashMap<>());
		ret.put(EXECUTABLE, new HashMap<>());

		data.getInt(); //length
		int num = data.getInt();

		for (int i = 0; i < num; i++) {
			ret.computeIfAbsent(BlorbChunkType.getType(data.getInt()), __ -> new HashMap<>())
			   .put(data.getInt(), data.getInt());
		}

		return ret;
	}

	/**
	 * Get the picture from the given ID.
	 *
	 * @param id The ID of the desired image.
	 * @return An {@link Optional} containing the desired picture or not
	 */
	public Optional<Image> getPicture(int id) {
		if (!fileIndex.get(PICTURE).containsKey(id)) {
			return Optional.empty();
		}
		return Optional.of(fileIndex.get(PICTURE).get(id).getAsImage());
	}

	public Optional<Chunk> getPictureChunk(int id) {
		if (!fileIndex.get(PICTURE).containsKey(id)) {
			return Optional.empty();
		}
		return Optional.of(fileIndex.get(PICTURE).get(id));
	}

	public int numPictures() {
		return fileIndex.get(PICTURE).size();
	}

	public Optional<AudioInputStream> getSound(int id) {
		if (!fileIndex.get(SOUND).containsKey(id)) {
			return Optional.empty();
		}
		return Optional.ofNullable(fileIndex.get(SOUND).get(id).getAsSound());
	}

	public int numSounds() {
		return fileIndex.get(SOUND).size();
	}

	public Optional<MemorySegment> getData(int id) {
		if (!fileIndex.get(DATA).containsKey(id)) {
			return Optional.empty();
		}
		return Optional.of(fileIndex.get(DATA).get(id).getData());
	}

	public Optional<MemorySegment> getDataWithHeader(int id) {
		if (!fileIndex.get(DATA).containsKey(id)) {
			return Optional.empty();
		}
		return fileIndex.get(DATA).get(id).getDataIncludingHeader();
	}

	public Optional<RepresentableID> getDataType(int id) {
		if (!fileIndex.get(DATA).containsKey(id)) {
			return Optional.empty();
		}
		return Optional.of(fileIndex.get(DATA).get(id).getID());
	}

	public int numData() {
		return fileIndex.get(DATA).size();
	}

	public @NotNull Optional<MemorySegment> getExec(int id) {
		if (!fileIndex.get(EXECUTABLE).containsKey(id)) {
			return Optional.empty();
		}
		return Optional.of(fileIndex.get(EXECUTABLE).get(id).getData());
	}

	public int numExec() {
		return fileIndex.get(EXECUTABLE).size();
	}

	public @NotNull MemorySegment getOptionalField(@NotNull BlorbChunkType type, int id) {
		if (!isOptionalType(type) || !fileIndex.containsKey(type)) {
			throw new IllegalArgumentException("Illegal optional type: " + type);
		}
		return fileIndex.get(type).get(id).getData();
	}

	public static boolean isOptionalType(BlorbChunkType id) {
		return switch (id) {
			case EXECUTABLE, PICTURE, SOUND -> false;
			default -> true;
		};
	}

	@NotNull
	public List<BlorbChunkType> getDefinedOptionals() {
		return optionalFieldsUsed;
	}

	@Override
	public String toString() {
		return "Resources{" +
				"fileIndex=" + fileIndex +
				", optionalFieldsUsed=" + optionalFieldsUsed +
				'}';
	}
}
