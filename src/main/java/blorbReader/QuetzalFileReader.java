/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package blorbReader;

import gameMemory.MemorySegment;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Path;
import java.util.EnumMap;
import java.util.Map;

public class QuetzalFileReader {
	private final Map<QuetzalChunkType, MemorySegment> chunks;

	public QuetzalFileReader(Path path) {
		MemorySegment bb;
		try (FileInputStream is = new FileInputStream(path.toFile())) {
			ByteBuffer b = ByteBuffer.allocateDirect((int) is.getChannel().size());
			is.getChannel().read(b);
			bb = MemorySegment.fromBuffer(b);
		} catch (IOException e) {
			throw new IllegalArgumentException("Unable to read from file" + e.getLocalizedMessage());
		}
		bb.rewind();
		if (QuetzalChunkType.FORM.getByteRepresentation() != bb.getInt()) {
			throw new IllegalArgumentException("FORM chuck was not found in file");
		}
		if (bb.length() != bb.getInt()) {
			throw new IllegalArgumentException("File size does not match header");
		}
		if (QuetzalChunkType.IFZS.getByteRepresentation() != bb.getInt()) {
			throw new IllegalArgumentException("Unable to find IFZS chunk");
		}
		chunks = new EnumMap<>(QuetzalChunkType.class);
		while (bb.hasRemaining()) {
			QuetzalChunkType ID = QuetzalChunkType.getType(bb.getInt());
			int size = bb.getInt();
			Chunk next = new MemorySegmentChunk(ID, bb.slice(bb.position(), size));
			chunks.put(((QuetzalChunkType) next.getID()), next.getData());
			bb.position(bb.position() + next.getLength());
		}
	}

	public Map<QuetzalChunkType, MemorySegment> getChunks() {
		return chunks;
	}
}
