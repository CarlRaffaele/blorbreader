/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package blorbReader;

import gameMemory.MemorySegment;
import javafx.scene.image.Image;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.sound.sampled.AudioInputStream;
import java.nio.ByteBuffer;
import java.util.Optional;

public interface Chunk {
	int getLength();

	@NotNull RepresentableID getID();

	MemorySegment getData();

	@NotNull Optional<MemorySegment> getDataIncludingHeader();

	Image getAsImage();

	String getImageBase64();

	@Nullable AudioInputStream getAsSound();

	void writeTo(MemorySegment destination);

	void writeTo(ByteBuffer destination);
}
