/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package blorbReader;

public enum QuetzalChunkType implements RepresentableID {
	FORM(0x464f524d),                    //FORM
	IF_HEADER(0x49466864),            //IFhd
	UNCOMPRESSED_MEMORY(0x554d656d),    //UMem
	COMPRESSED_MEMORY(0x434d656d),    //CMem
	STACKS(0x53746b73),                //Stks
	INTERPRETER_DEPENDENT(0x496e7444), //IntD
	IFZS(0x49465a53),                    //IFZS (FORM type)

	AUTHOR(0x41555448),                //AUTH
	COPYRIGHT(0x2863295f),            //(c)_
	ANNOTATION(0x414e4e4f),            //ANNO
	;

	private final int BYTE_REPRESENTATION;

	QuetzalChunkType(int byteRepresentation) {
		BYTE_REPRESENTATION = byteRepresentation;
	}

	public static QuetzalChunkType getType(int byteRepresentation) {
		return switch (byteRepresentation) {
			case 0x554d656d -> UNCOMPRESSED_MEMORY;
			case 0x434d656d -> COMPRESSED_MEMORY;
			case 0x53746b73 -> STACKS;
			case 0x41555448 -> AUTHOR;
			case 0x2863295f -> COPYRIGHT;
			case 0x414e4e4f -> ANNOTATION;
			case 0x496e7444 -> INTERPRETER_DEPENDENT;
			case 0x49465a53 -> IFZS;
			case 0x464f524d -> FORM;
			case 0x49466864 -> IF_HEADER;
			default -> throw new IllegalArgumentException("Unknown byte representation: " + byteRepresentation);
		};
	}

	@Override
	public int getByteRepresentation() {
		return BYTE_REPRESENTATION;
	}
}
