/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package blorbReader;

import gameMemory.MemorySegment;
import javafx.scene.image.Image;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Optional;

class MemorySegmentChunk extends BaseChunk {
	@NotNull private final MemorySegment data;
	@Nullable private final MemorySegment dataWithHeader;
	private Image image;
//	private String base64;

	MemorySegmentChunk(@NotNull RepresentableID ID, @NotNull MemorySegment data) {
		super(ID, data.length());
		this.data = data;
		dataWithHeader = null;
		image = null;
//		base64 = null;
	}

	MemorySegmentChunk(@NotNull MemorySegment data) {
		super(BlorbChunkType.getType(data.getInt()), data.getInt());
		this.data = data.slice(8, length);
		dataWithHeader = data.slice(0, length + 8);
		image = null;
//		base64 = null;
	}

	@Override
	public MemorySegment getData() {
		return data.rewind();
	}

	@Override
	public @NotNull Optional<MemorySegment> getDataIncludingHeader() {
		return Optional.ofNullable(dataWithHeader);
	}

	@Override
	public Image getAsImage() {
		if (image == null) {
			image = new Image(data.asInputStream());
		}
		return image;
	}

	@Override
	public String getImageBase64() {
//		if (base64 == null) {
//			String mime;
//			try {
//				mime = URLConnection.guessContentTypeFromStream(data.asInputStream());
//			} catch (IOException e) {
//				e.printStackTrace();
//				mime = "";
//			}
//			base64 = String.format("data:%s;base64,%s",
//					mime,
//					new String(Base64.getEncoder().encode(data.rewind()).array(), StandardCharsets.US_ASCII));
//		}
//		return base64;
		return "";
	}

	@Override
	public @Nullable AudioInputStream getAsSound() {
		InputStream ais = data.asInputStream();
		AudioFileFormat audioFileFormat;
		try {
			audioFileFormat = AudioSystem.getAudioFileFormat(ais);
		} catch (UnsupportedAudioFileException | IOException e) {
			return null;
		}
		return new AudioInputStream(ais, audioFileFormat.getFormat(), audioFileFormat.getFrameLength());
	}

	@Override
	public void writeTo(MemorySegment destination) {
		data.rewind();
		destination.put(ID.getByteRepresentation());
		destination.put(length);
		destination.put(data);
	}

	@Override
	public void writeTo(ByteBuffer destination) {
		writeTo(MemorySegment.fromBuffer(destination));
	}

	@Override
	public String toString() {
		return "BaseChunk{" +
				"length=" + length +
				", ID=" + ID +
				'}';
	}
}
