/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package blorbReader;

import gameMemory.MemorySegment;
import org.jetbrains.annotations.NotNull;

import java.nio.charset.StandardCharsets;
import java.util.Locale;

/**
 * Class to initially read a Ulx file and start executing the Glux instructions.
 */
public class UlxReader {
	private final MemorySegment gameData;
	private final @NotNull GlulxHeader gluxHeader;
	private final @NotNull GlulxDebuggingHeader gluxDebuggingHeader;

	public UlxReader(MemorySegment file) {
		gameData = file;
		this.gluxHeader = parseGluxHeader(this.gameData);
		this.gluxDebuggingHeader = parseDebugHeader(this.gameData);
	}

	/**
	 * Parse out Glux header information using the game data. The data must be at the start of the header information
	 *
	 * @return A map containing the different header entries as String-keys and Integer values
	 */
	@NotNull
	private static GlulxHeader parseGluxHeader(@NotNull MemorySegment gameData) {
		byte[] temp = new byte[4];
		gameData.getBytes(temp, 0, 4);
		if (!new String(temp, StandardCharsets.US_ASCII).toLowerCase(Locale.US).equals("glul")) {
			throw new IllegalArgumentException();
		}
		return new GlulxHeader(gameData.getInt(), gameData.getInt(), gameData.getInt(), gameData.getInt(),
				gameData.getInt(), gameData.getInt(), gameData.getInt(), gameData.getInt());
	}

	@NotNull
	private static GlulxDebuggingHeader parseDebugHeader(@NotNull MemorySegment gameData) {
		byte[] temp = new byte[4];
		gameData.getBytes(temp, 0, 4);
		if (!new String(temp, StandardCharsets.US_ASCII).toLowerCase(Locale.US).equals("info")) {
			throw new IllegalArgumentException();
		}

		return new GlulxDebuggingHeader(gameData.getInt(), gameData.getInt(), gameData.getInt(), gameData.getShort(),
				((((long) gameData.getInt()) << 16) | gameData.getShort()));
	}

	public MemorySegment getGameData() {
		return gameData;
	}

	public @NotNull GlulxDebuggingHeader getGluxDebuggingHeader() {
		return gluxDebuggingHeader;
	}

	public @NotNull GlulxHeader getGluxHeader() {
		return gluxHeader;
	}
}
