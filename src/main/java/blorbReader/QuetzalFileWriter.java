/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package blorbReader;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.jetbrains.annotations.Contract;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;

public class QuetzalFileWriter extends QuetzalByteWriter {
	private final Path file;

	@Contract(pure = true)
	public QuetzalFileWriter(Path file) {
		super();
		this.file = file;
	}

	@SuppressFBWarnings({"RCN_REDUNDANT_NULLCHECK_OF_NONNULL_VALUE"})
	public void write() throws IOException {
		if (!open) {
			throw new NullPointerException("Tried to add a chunk to a closed writer");
		}

		int size = chunks.stream().mapToInt(Chunk::getLength).sum();
		int totalSize = size + 12 + (8 * chunks.size());
		try (RandomAccessFile f = new RandomAccessFile(file.toFile(), "rw")) {
			MappedByteBuffer bb = f.getChannel().map(FileChannel.MapMode.READ_WRITE, 0, totalSize);
			//Write the FORM header
			bb.putInt(QuetzalChunkType.FORM.getByteRepresentation());
			bb.putInt(totalSize);
			bb.putInt(QuetzalChunkType.IFZS.getByteRepresentation());
			chunks.forEach(c -> c.writeTo(bb));
		} catch (IOException e) {
			Files.deleteIfExists(file);
			throw e;
		}
		open = false;
	}
}
