/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package blorbReader;

import gameMemory.MemorySegment;
import javafx.scene.image.Image;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.sound.sampled.AudioInputStream;
import java.util.Optional;

public abstract class BaseChunk implements Chunk {
	protected final int length;
	@NotNull protected final RepresentableID ID;

	protected BaseChunk(@NotNull RepresentableID id, int length) {
		this.length = length;
		ID = id;
	}

	@Override
	public int getLength() {
		return length;
	}

	@Override
	public @NotNull RepresentableID getID() {
		return ID;
	}

	@Override
	public MemorySegment getData() {
		throw new UnsupportedOperationException();
	}

	@Override
	public @NotNull Optional<MemorySegment> getDataIncludingHeader() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Image getAsImage() {
		throw new UnsupportedOperationException();
	}

	@Override
	public String getImageBase64() {
		throw new UnsupportedOperationException();
	}

	@Override
	public @Nullable AudioInputStream getAsSound() {
		throw new UnsupportedOperationException();
	}
}
