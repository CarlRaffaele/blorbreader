/*
 * Copyright (C) 2021 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package blorbReader;

public enum BlorbChunkType implements RepresentableID {
	//Form IDs
	FORM(0x464f524d),            //FORM
	IFRS(0x49465253),
	RESOURCE_INDEX(0x52496478),//RIdx
	PICTURE(0x50696374),        //Pict
	SOUND(0x536e6420),            //Snd
	EXECUTABLE(0x45786563),    //Exec
	DATA(0x44617461),            //Data
	//Chunk IDs
	PICTURE_PNG(0x504E4720),    //PNG
	PICTURE_JPEG(0x4a504547),    //JPEG
	//	AIFF, //The chunk is a FORM type with an AIFF chunk inside
	SOUND_MOD(0x4d4f4420),        //MOD
	SOUND_SONG(0x534f4e47),    //SONG
	EXEC_ZCOD(0x5a434f44),        //ZCOD
	EXEC_GLUL(0x474c554c),        //GLUL
	TEXT(0x54455854),            //TEXT
	//Optional
	COLOR_PALETTE(0x506c7465),    //Plte
	RESOLUTION(0x5265736f),    //Reso
	LOOP(0x4c6f6f70),            //Loop
	RELEASE_NUMBER(0x52656c4e),//RelN
	IF_HEADER(0x49466864),    //IFhd
	//Optional, in many IFF FORMs
	AUTHOR(0x41555448),        //AUTH
	COPYRIGHT(0x2863295f),    //(c)_
	ANNOTATION(0x414e4e4f),    //ANNO
	;
	private final int BYTE_REPRESENTATION;

	BlorbChunkType(int byte_representation) {
		BYTE_REPRESENTATION = byte_representation;
	}

	public static BlorbChunkType getType(int byteRepresentation) {
		return switch (byteRepresentation) {
			//Blorb chunks
			case 0x49466864 -> IF_HEADER;
			case 0x464f524d -> FORM;
			case 0x49465253 -> IFRS;
			case 0x52496478 -> RESOURCE_INDEX;
			case 0x50696374 -> PICTURE;
			case 0x536e6420 -> SOUND;
			case 0x45786563 -> EXECUTABLE;
			case 0x44617461 -> DATA;
			case 0x506c7465 -> COLOR_PALETTE;
			case 0x5265736f -> RESOLUTION;
			case 0x4c6f6f70 -> LOOP;
			case 0x52656c4e -> RELEASE_NUMBER;
			case 0x504E4720 -> PICTURE_PNG;
			case 0x4a504547 -> PICTURE_JPEG;
			case 0x4d4f4420 -> SOUND_MOD;
			case 0x534f4e47 -> SOUND_SONG;
			case 0x5a434f44 -> EXEC_ZCOD;
			case 0x474c554c -> EXEC_GLUL;
			default -> throw new IllegalArgumentException("Unknown byte representation: " + byteRepresentation);
		};
	}

	@Override
	public int getByteRepresentation() {
		return BYTE_REPRESENTATION;
	}
}
