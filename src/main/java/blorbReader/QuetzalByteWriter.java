/*
 * Copyright (C) 2020 Carl Raffaele Jr.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package blorbReader;

import gameMemory.MemorySegment;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayDeque;
import java.util.Deque;

import static blorbReader.QuetzalChunkType.FORM;
import static blorbReader.QuetzalChunkType.IFZS;

public class QuetzalByteWriter {
	final Deque<Chunk> chunks;
	boolean open;

	@Contract(pure = true)
	public QuetzalByteWriter() {
		chunks = new ArrayDeque<>();
		open = true;
	}

	public void addChunk(@NotNull QuetzalChunkType type, @NotNull MemorySegment data) {
		if (!open) {
			throw new NullPointerException("Tried to add a chunk to a closed writer");
		}
		chunks.push(new MemorySegmentChunk(type, data));
	}

	private int getNumberBytes() {
		int size = chunks.stream().mapToInt(Chunk::getLength).sum();
		return size +
				12 + //header
				(8 * chunks.size()); //Chunk header - 4 bytes for type and 4 for size
	}

	public MemorySegment toSegment() {
		MemorySegment ret = MemorySegment.fromSize(getNumberBytes());
		ret.put(FORM.getByteRepresentation());
		ret.put(ret.length());
		ret.put(IFZS.getByteRepresentation());
		chunks.forEach(c -> c.writeTo(ret));
		return ret.rewind();
	}
}
